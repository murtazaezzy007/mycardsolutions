<?php 
    $active = 'savings-account';
?>
<?php require_once './layout/header.php'; ?>


    <div class="container m-t-20 m-b-40">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Deposit Products</a></li>
                    <li class="active">Savings Bank account</li>
                </ol>
            </div>
            <div class="col-md-3 col-xs-12">
                <?php require_once './layout/nav-sidebar.php'; ?>
            </div>
            <div class="col-md-9 content col-xs-12">
                <h1>Savings Bank Account</h1>

                <h4 class="m-t-30">Know Your Customer Guidelines</h4>
                <p>Any person fulfilling account opening requirements may, upon agreeing to comply with the prescribed rules, open a Savings Bank Account, provided (Rule No. 1)</p>

                <ul class="alphabetical-list m-t-20">
                    <li>In case of an individual, who is eligible to be enrolled for an Aadhaar number, he shall submit the Aadhaar number issued by UIDAI and PAN or Form 60 at the time of opening an account based relationship w.e.f. 01.06.2017.</li>
                    <li>All new accounts are required to be opened with Aadhaar and Permanent Account number (PAN) or Form 60 as KYC document.</li>
                    <li>However, where Aadhaar number has not been assigned to an applicant, the applicant shall furnish proof of application of Enrolment for Aadhaar and in case PAN is not submitted, one certified copy of an official Valid Document (OVD) along with form 60 is required.</li>
                    <li>In case the applicant is not a resident or is a resident in the states of Jammu and Kashmir, Assam or Meghalaya and does not submit the PAN, he shall submit one certified copy of OVD containing details</li>
                    <li>The extant instructions regarding opening of BSBDA-Small Account will continue.</li>
                </ul>

                <h4 class="m-t-30">Nomination and Survivorship Facility</h4>
                <p>The nomination facility is available on Savings Bank Accounts and the account holders are advised to avail of this facility for smooth settlement of claim by legal heirs in unforeseen circumstances. Nomination can be made in favour of only one nominee. In case they do not wish to make a nomination, the fact should be recorded on the account opening form under their full signature. Joint account with survivorship benefit can be operated by the survivor, in such circumstances. </p>

                <h4 class="m-t-30">Interest</h4>
                <p>The Rate of interest is 4% paid on daily balance of Savings Bank account</p>

                <div class="row m-t-30">
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <h4>Type of Account</h4>
                        <p>With Cheque book facility</p>
                        <p>Without Cheque book facility</p>
                    </div>
                    <div class="col-md-8 col-sm-6 col-xs-6">
                        <h4>Minimum Balance</h4>
                        <p>1000/-</p>
                        <p>500/-</p>
                    </div>
                </div>

                <table class="table table-bordered customised-table m-t-22">
                    <tbody>
                        <tr>
                            <th>Purpose</th>
                            <td>Savings</td>
                        </tr>
                        <tr>
                            <th>Account type.</th>
                            <td>Operative account with or without cheque</td>
                        </tr>
                        <tr>
                            <th>Elibility.</th>
                            <td>Any individual or joint individuals, Trusts and associations who are not doing business activities.</td>
                        </tr>
                        <tr>
                            <th>Number of joint customers.</th>
                            <td>Maximum Three.</td>
                        </tr>
                        <tr>
                            <th>Minimum age to account holders.</th>
                            <td>No age limit.</td>
                        </tr>
                        <tr>
                            <th>Minimum amount to open an account.</th>
                            <td>Rs.1,000.</td>
                        </tr>
                        <tr>
                            <th>Maximum deposit.</th>
                            <td>No limit.</td>
                        </tr>
                        <tr>
                            <th>Rate of interest.</th>
                            <td>Based on period of deposit.</td>
                        </tr>
                        <tr>
                            <th>Nomination facility</th>
                            <td>Mandatory</td>
                        </tr>
                        <tr>
                            <th>TDS applicability</th>
                            <td>N/A</td>
                        </tr>
                        <tr>
                            <th>Form 15G/H</th>
                            <td>N/A</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php require_once './layout/footer.php'; ?>