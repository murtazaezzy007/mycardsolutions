<?php 
    $active = 'cash-certificate-deposit';
?>
<?php require_once './layout/header.php'; ?>

    <div class="container m-t-20 m-b-40">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Deposit Products</a></li>
                    <li class="active">Cash Certificate Deposit</li>
                </ol>
            </div>
            <div class="col-md-3 col-xs-12">
                <?php require_once './layout/nav-sidebar.php'; ?>
            </div>
            <div class="col-md-9 content col-xs-12">
                <h1>Cash Certificate Deposit &nbsp;&nbsp;<small>(Cumulative Interest)</small></h1>
                <table class="table table-bordered customised-table m-t-22">
                    <tbody>
                    <?php 
                        $arr = array(
                            array('name' => 'Purpose', 'value' => 'Savings'), 
                            array('name' => 'Account type.', 'value' => 'Any individual, joint, business customer.'), 
                            array('name' => 'Minimum age to account holders', 'value' => 'No age limit.'),
                            array('name' => 'Minimum period of deposit.', 'value' => '15 days.'), 
                            array('name' => 'Maximum period of deposit.', 'value' => '10 Years.'), 
                            array('name' => 'Minimum amount to open an account', 'value' => 'Rs.5000.'), 
                            array('name' => 'Maximum deposit', 'value' => 'No limit.'), 
                            array('name' => 'Interest calculation method', 'value' => 'Compounded quarterly if deposit period is at least 6 months.'),
                            array('name' => 'Interest payment', 'value' => 'At the time of maturity.'),
                            array('name' => 'Before maturity facility.', 'value' => 'Available with penalty as applicable.'),
                            array('name' => 'Nomination facility', 'value' => 'Mandatory'),
                            array('name' => 'TDS applicability', 'value' => 'General - Rs.10000/- & above - Senior Citizen - Rs.50000/- & above'),
                            array('name' => 'Form 15G/H', 'value' => 'Accepted provided submitted within 15 days of given financial year.'),
                            array('name' => 'Form 15 G/H periodicity', 'value' => 'Must be submitted during each and every new FD opening.')
                        );
                        for($i = 0; $i < count($arr); $i++):
                        ?>
                        <tr>
                            <th><?php echo $arr[$i]['name']; ?></th>
                            <td><?php echo $arr[$i]['value']; ?></td>
                        </tr>
                        <?php endfor; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php require_once './layout/footer.php'; ?>