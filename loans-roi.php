<?php require_once './layout/header.php'; ?>
    <div class="mypage-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li class="active">Loans</li>
                    </ol>

                    <h1>Loans - Rate of Interest</h1>
                    <hr class="short orange">
                </div>
            </div>
        </div><!-- /.container -->
    </div>


    <div class="container m-t-40 m-b-40">
        <div class="row">
            <div class="col-md-8">
                <table class="table table-bordered customised-table">
                    <tbody>
                        <tr>
                            <th colspan="2">Period</th>
                            <th colspan="2"><b>Rate of Interest</b></th>
                        </tr>
                        <tr>
                            <th><b>From</b></th>
                            <th><b>To</b></th>
                            <th><b>Effective From</b></th>
                            <th><b>01.12.2016</b></th>
                        </tr>
                        <tr>
                            <td>0 days</td>
                            <td>14 days</td>
                            <td colspan="2">0%</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <?php require_once './layout/newsboard.php'; ?>
            </div>
        </div>
    </div>
    

<?php require_once './layout/footer.php'; ?>