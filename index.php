<?php require_once './layout/header.php'; ?>
<?php $hideFooterImg = true; ?>
<div class="homepage top-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <h2 class="banner-text">HOME LOANS</h2>
                <h5 class="sub-banner-text">at attractive interest rates </h5>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 small-box-container">
                <div class="small-box">
                    <div class="small-box-header">ONLINE BANKING</div>
                    <div class="small-box-content">
                    <a href="login.php">
                    <button type="button" class="btn btn-primary text-uppercase call-to-action-button">Customer Login</button>
                    </a>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid homepage marquee-wrapper">
    <marquee behavior="" direction="">
        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </span>
        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </span>
    </marquee>
</div>

<div class="container homepage-content">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="newsboard-section">
                <h4>News board</h4>

                <div class="row">
                    <div class="col-md-6">
                        <ul class="newsboard-section-list list-unstyled">
                            <li><i class="fa fa-angle-right"></i>Lorem ipsum dolor sit amet, lorem ipsum dolor sit sed do eiusmod tempor incididunt ut.</li>
                            <li><i class="fa fa-angle-right"></i>Lorem ipsum dolor sit amet, lorem ipsum dolor sit sed do eiusmod tempor incididunt ut.</li>
                            <li><i class="fa fa-angle-right"></i>Lorem ipsum dolor sit amet, lorem ipsum dolor sit sed do eiusmod tempor incididunt ut.</li>
                            <li><i class="fa fa-angle-right"></i>Lorem ipsum dolor sit amet, lorem ipsum dolor sit sed do eiusmod tempor incididunt ut.</li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="hot-links">
                            <span class="medium-text">Go anywhere with</span> <h4 class="has-underline big-text">RUPAY DEBIT CARD</h4>
                            <button type="button" class="btn btn-primary">GET RUPAY NOW</button>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="hot-links">
                            <span class="medium-text"> Search your</span> <h4 class="has-underline big-text text-uppercase">IFSC / MICR Codes</h4>
                            <button type="button" class="btn btn-primary text-uppercase">Go search now</button>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
</div>


            <div class="punch-line m-b-40">
                <h4><span>Customer Service</span> is our tradition</h4>
                <p>Our customer service is among the best in the country.</p>
            </div>

<div class="container homepage-content">
    <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="newsboard-section">
                <h4 class="m-b-20 text-uppercase">Important Links</h4>
                <ul class="newsboard-section-list list-unstyled">
                    <li>
                        <i class="fa fa-angle-right"></i>
                        <a href="#">Rupay/ATM Card Apply by online</a>
                    </li>
                    <li>
                        <i class="fa fa-angle-right"></i>
                        <a href="#">SMS Registration</a>
                    </li>
                    <li>
                        <i class="fa fa-angle-right"></i>
                        <a href="#">Mobile No. Registration</a>
                    </li>
                    <li>
                        <i class="fa fa-angle-right"></i>
                        <a href="#">E-mail Registration</a>
                    </li>
                    <li>
                        <i class="fa fa-angle-right"></i>
                        <a href="#">GST No. Registration</a>
                    </li>
                    <li>
                        <i class="fa fa-angle-right"></i>
                        <a href="#">CKYC Registration</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-8 col-sm-6 col-xs-12">
            <div class="newsboard-section">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                    
                        <h4 class="m-b-20 text-uppercase">Financial Information</h4>
                        <ul class="newsboard-section-list list-unstyled">
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="#">Financial Reports</a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="#">Gen. Body Meeting Report</a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="#">Audit Report</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="hot-links">
                            <span class="medium-text">Go anywhere with</span> <h4 class="has-underline big-text text-uppercase">Matured Deposit</h4>
                            <button type="button" class="btn btn-primary">KNOW MORE</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row about-us">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <img src="assets/images/bank-building.jpeg" class="img img-responsive" alt="Bank Building">
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <h3 class="has-underline bigger-text">About Us</h3>
            <p>Lorem ipsum dolor sit met, consecteteur dui sius et lit amet merolket.</p>
            <button type="button" class="btn btn-primary btn-lg">KNOW MORE</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12 plain-title">
            Why Choose us
        </div>
        <div class="col-md-12">
            
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 choose-section">
            <img src="assets/images/hand-shake.jpg" class="img img-responsive" alt="">
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 choose-section">
            <h3 class="has-underline choose-section-title">5 Decades of serving</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
        </div>
        <div class="clearfix hidden-lg hidden-md m-b-30"></div>
        <div class="col-md-4 col-sm-6 col-xs-12 choose-section">
            <img src="assets/images/online-payment.jpeg" class="img img-responsive" alt="">
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 choose-section">
            <h3 class="has-underline choose-section-title">Hassle free banking</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
        </div>
        <div class="clearfix hidden-lg hidden-md m-b-30"></div>
        <div class="col-md-4 col-sm-6 col-xs-12 choose-section">
            <img src="assets/images/girl-online.jpeg" class="img img-responsive" alt="">
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 choose-section">
            <h3 class="has-underline choose-section-title">Your solutions partner</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="newsboard-section">
                <h4 class="m-b-20 text-uppercase">Quick Links</h4>
                <ul class="newsboard-section-list list-unstyled">
                    <li>
                        <i class="fa fa-angle-right"></i>
                        <a href="#">Bank Holiday List</a>
                    </li>
                    <li>
                        <i class="fa fa-angle-right"></i>
                        <a href="#">Banking Working Hours</a>
                    </li>
                    <li>
                        <i class="fa fa-angle-right"></i>
                        <a href="#">Forms & Related links</a>
                    </li>
                    <li>
                        <i class="fa fa-angle-right"></i>
                        <a href="#">KSC Federation</a>
                    </li>
                    <li>
                        <i class="fa fa-angle-right"></i>
                        <a href="#">Sahakera Sindhu</a>
                    </li>
                    <li>
                        <i class="fa fa-angle-right"></i>
                        <a href="#">Sahakera Daspana</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="newsboard-section">
                <h4 class="m-b-20 text-uppercase">Related Links</h4>
                <ul class="newsboard-section-list list-unstyled">
                    <li>
                        <i class="fa fa-angle-right"></i>
                        <a href="http://kubfed.com">kubfed.com</a>
                    </li>
                    <li>
                        <i class="fa fa-angle-right"></i>
                        <a href="http://sahakara.kar.gov.in">sahakara.kar.gov.in</a>
                    </li>
                    <li>
                        <i class="fa fa-angle-right"></i>
                        <a href="http://www.incometaxindiaefiling.gov.in">www.incometaxindiaefiling.gov.in</a>
                    </li>
                    <li>
                        <i class="fa fa-angle-right"></i>
                        <a href="http://sahakaradarpana.kar.nic.in">sahakaradarpana.kar.nic.in</a>
                    </li>
                    <li>
                        <i class="fa fa-angle-right"></i>
                        <a href="http://rbi.org.in">rbi.org.in</a>
                    </li>
                    <li>
                        <i class="fa fa-angle-right"></i>
                        <a href="http://www.shcilestamp.com">www.shcilestamp.com</a>
                    </li>
                </ul>
            </div>
        </div>
        
    </div>
</div>
    
<?php require_once './layout/footer.php'; ?>