<?php require_once './layout/header.php'; ?>
    <div class="mypage-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Library</a></li>
                        <li class="active">Data</li>
                    </ol>

                    <h1>Branch Network</h1>
                    <hr class="short orange">
                </div>
            </div>
        </div><!-- /.container -->
    </div>


    <div class="container m-t-40 m-b-40">
        <div class="row">
            <?php 
            // $arr = array(
            //     array(
            //         'branch'=> 'P J EXTENSION, DAVANGERE',
            //         'license'=> 'U.B.D.K.A. 1032 P. Dt.22.10.1990',
            //         'address'=> '#165/1-2, 3rd Main, P.J. Extention, Vasavi Road, DAVANGERE-577001',
            //         'telephone'=> '08192-251653',
            //         'designation'=> 'GENERAL MANAGER',
            //         'name'=> 'SRI AMBIKAPATHY SETTY D H',
            //         'mobile'=> '98801 32316         ',
            //         'image'=> 'assets/images/staff/img1.png'
            //     ),
            //     array(
            //         'branch'=> 'P J EXTENSION, DAVANGERE',
            //         'license'=> 'U.B.D.K.A. 1032 P. Dt.22.10.1990',
            //         'address'=> '#165/1-2, 3rd Main, P.J. Extention, Vasavi Road, DAVANGERE-577001',
            //         'telephone'=> '08192-251653',
            //         'designation'=> 'BRANCH MANAGER',
            //         'name'=> 'SRI BHEEMANANDA SETTY',
            //         'mobile'=> '95135 63421',
            //         'image'=> 'assets/images/staff/img2.png'
            //     )
            // );

            $arr = array(
                array(
                    'name' => 'SRI AMBIKAPATHY SETTY D H',
                    'role' => 'GENERAL MANAGER',
                    'contact' => '98801 32316',
                    'image'=> 'assets/images/staff/img1.png'
                ),
                array(
                    'name' => 'SRI BHEEMANANDA SETTY',
                    'role' => 'BRANCH MANAGER',
                    'contact' => '95135 63421',
                    'image'=> 'assets/images/staff/img2.png'
                ),
                array(
                    'name' => 'SRI PRASAD P V',
                    'role' => 'BRANCH MANAGER',
                    'contact' => '95135 63422',
                    'image'=> 'assets/images/staff/img3.png'
                ),
                array(
                    'name' => 'SMT PARVATHAMMA R L',
                    'role' => 'BRANCH MANAGER',
                    'contact' => '95135 63423',
                    'image'=> 'assets/images/staff/img4.png'
                ),
                array(
                    'name' => 'SRI VIJAYA KUMAR S R',
                    'role' => 'BRANCH MANAGER',
                    'contact' => '95135 63424',
                    'image'=> 'assets/images/staff/img5.png'
                ),
                array(
                    'name' => 'SRI PRASAD SETTY N',
                    'role' => 'BRANCH MANAGER',
                    'contact' => '95135 63425',
                    'image'=> 'assets/images/staff/img6.png'
                ),
                array(
                    'name' => 'SRI SANDESHA TEEKA',
                    'role' => 'SPECIAL ASST. OFFICER',
                    'contact' => '95135 63433',
                    'image'=> 'assets/images/staff/img7.png'
                ),
                array(
                    'name' => 'SMT SHANTHALA B A',
                    'role' => 'SPECIAL ASST. OFFICER',
                    'contact' => '95135 63426',
                    'image'=> 'assets/images/staff/img8.png'
                ),
                array(
                    'name' => 'SMT NAGAJYOTHI',
                    'role' => 'SPECIAL ASST. OFFICER',
                    'contact' => '95135 63427',
                    'image'=> 'assets/images/staff/img9.png'
                ),
                array(
                    'name' => 'SRI NANJUNDESHWARA M V',
                    'role' => 'SPECIAL ASST. OFFICER',
                    'contact' => '95135 63429',
                    'image'=> 'assets/images/staff/img10.png'
                )
            );

            $staff_members = array(
                array(
                    'name' => 'SRI SUBRAMANYA A P',
                    'role' => 'JUNIOR CLERK',
                    'contact' => '95135 63431',
                    'image'=> 'https://via.placeholder.com/210x242'
                ),
                array(
                    'name' => 'SRI MALLESH B J',
                    'role' => 'JUNIOR CLERK',
                    'contact' => '95135 63428',
                    'image'=> 'https://via.placeholder.com/210x242'
                ),
                array(
                    'name' => 'SRI TEJASWI K R',
                    'role' => 'JUNIOR CLERK',
                    'contact' => '81050 89968',
                    'image'=> 'https://via.placeholder.com/210x242'
                ),
                array(
                    'name' => 'SMT KAVITHA',
                    'role' => 'JUNIOR CLERK',
                    'contact' => '9513563430',
                    'image'=> 'https://via.placeholder.com/210x242'
                ),
                array(
                    'name' => 'SMT TEJASWINI K P',
                    'role' => 'JUNIOR CLERK',
                    'contact' => '95916 91214',
                    'image'=> 'https://via.placeholder.com/210x242'
                ),
                array(
                    'name' => 'SRI VISHWANATHA C',
                    'role' => 'JUNIOR CLERK',
                    'contact' => '85536 59220',
                    'image'=> 'https://via.placeholder.com/210x242'
                ),
                array(
                    'name' => 'SRI RANGANATHA G',
                    'role' => 'JUNIOR CLERK',
                    'contact' => '95135 63420',
                    'image'=> 'https://via.placeholder.com/210x242'
                ),
                array(
                    'name' => 'SRI HARSHA K N',
                    'role' => 'JUNIOR CLERK',
                    'contact' => '81234 63616',
                    'image'=> 'https://via.placeholder.com/210x242'
                ),
                array(
                    'name' => 'SRI RAKESHA',
                    'role' => 'JUNIOR CLERK',
                    'contact' => '95386 04438',
                    'image'=> 'https://via.placeholder.com/210x242'
                ),
                array(
                    'name' => 'SRI SACHIN Y R',
                    'role' => 'JUNIOR CLERK',
                    'contact' => '89512 28861',
                    'image'=> 'https://via.placeholder.com/210x242'
                ),
                array(
                    'name' => 'SRI ARUNA R',
                    'role' => 'JUNIOR CLERK',
                    'contact' => '72046 16772',
                    'image'=> 'https://via.placeholder.com/210x242'
                ),
                array(
                    'name' => 'SRI TEJAS GUPTA K S',
                    'role' => 'JUNIOR CLERK',
                    'contact' => '76192 61015',
                    'image'=> 'https://via.placeholder.com/210x242'
                ),
                array(
                    'name' => 'SRI KHARTE L R',
                    'role' => 'PEON',
                    'contact' => '72046 16772',
                    'image'=> 'https://via.placeholder.com/210x242'
                ),
                array(
                    'name' => 'SRI NAGARAJ N',
                    'role' => 'PEON',
                    'contact' => '95135 63432',
                    'image'=> 'https://via.placeholder.com/210x242'
                ),
                array(
                    'name' => 'SRI SHABBIR AHAMED',
                    'role' => 'PEON CUM DRIVER',
                    'contact' => '76192 61011',
                    'image'=> 'https://via.placeholder.com/210x242'
                ),
                array(
                    'name' => 'SRI MADHU C',
                    'role' => 'PEON',
                    'contact' => '76192 61013',
                    'image'=> 'https://via.placeholder.com/210x242'
                ),
                array(
                    'name' => 'SRI MANJUNATHA SETTY V M',
                    'role' => 'PEON',
                    'contact' => '76192 61014',
                    'image'=> 'https://via.placeholder.com/210x242'
                )
            );

            for($i = 0; $i < count($arr); $i++):
            ?>
            
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="description-box">
                    <div class="description-row">
                        <div class="description-title">Branch Name</div>
                        <div class="description-value"> P J EXTENSION, DAVANGERE</div>
                    </div>
                    <div class="description-row">
                        <div class="description-title">RBI License</div>
                        <div class="description-value">U.B.D.K.A. 1032 P. Dt.22.10.1990</div>
                    </div>
                    <div class="description-row">
                        <div class="description-title">Address</div>
                        <div class="description-value">#165/1-2, 3rd Main, P.J. Extention, Vasavi Road, DAVANGERE-577001</div>
                    </div>
                    <div class="description-row">
                        <div class="description-title">Telephone</div>
                        <div class="description-value">08192-251653</div>
                    </div>
                    <div class="description-title text-center"><?php echo $arr[$i]['role']; ?></div>
                    <div class="description-row has-image">
                        <img src="<?php echo $arr[$i]['image']; ?>" alt="">
                        <div class="description-value">
                            <div class="description-row has-image">
                                <div class="description-title p-0">Name</div>
                                <div class="description-value p-0"><?php echo $arr[$i]['name']; ?></div>
                            </div>
                            <div class="description-row">
                                <div class="description-title p-0">Mobile</div>
                                <div class="description-value p-0"><?php echo $arr[$i]['contact']; ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php endfor; ?>
        </div>

        
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <table class="table table-bordered customised-table m-t-22">
                    <tbody>
                        <tr>
                            <th class="col-md-4">Name</th>
                            <th class="col-md-4">Position</th>
                            <th class="col-md-4">Mobile No.</th>
                        </tr>
                        <?php 
                        for($i = 0; $i < count($staff_members); $i++):
                        ?>
                        <tr>
                            <td><?php echo $staff_members[$i]['name']; ?></td>
                            <td><?php echo $staff_members[$i]['role']; ?></td>
                            <td><?php echo $staff_members[$i]['contact']; ?></td>
                        </tr>
                        <?php endfor; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    

<?php require_once './layout/footer.php'; ?>