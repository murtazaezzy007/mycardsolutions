<?php 
    $active = 'pigmy-deposit';
?>
<?php require_once './layout/header.php'; ?>

    <div class="container m-t-20 m-b-40">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Deposit Products</a></li>
                    <li class="active">Pigmy Deposit</li>
                </ol>
            </div>
            <div class="col-md-3 col-xs-12">
                <?php require_once './layout/nav-sidebar.php'; ?>
            </div>
            <div class="col-md-9 content col-xs-12">
                <h1>Pigmy Deposit</h1>
                <div class="btn-sub-header-right">
                    <button type="button" class="btn btn-primary">Deposit Interest Calculator</button>
                </div>
                <table class="table table-bordered customised-table m-t-22">
                    <tbody>
                    <?php 
                        $arr = array(
                            array('name' => 'Purpose', 'value' => 'Savings'), 
                            array('name' => 'Account type', 'value' => 'Any individual, joint, business customer.'), 
                            array('name' => 'Minimum age to account holders', 'value' => 'No age limit.'),
                            array('name' => 'Maximum period of deposit.', 'value' => 'Six Months.'), 
                            array('name' => 'Minimum amount to open an accountn', 'value' => 'Rs.100.'), 
                            array('name' => 'Maximum deposit', 'value' => 'No limit.'), 
                            array('name' => 'Before maturity facility.', 'value' => 'Available with penalty as applicable.'), 
                            array('name' => 'Nomination facility', 'value' => 'Mandatory')
                        );
                        for($i = 0; $i < count($arr); $i++):
                        ?>
                        <tr>
                            <th><?php echo $arr[$i]['name']; ?></th>
                            <td><?php echo $arr[$i]['value']; ?></td>
                        </tr>
                        <?php endfor; ?>
                    </tbody>
                </table>
                <p>* T&C Apply</p>
            </div>
        </div>
    </div>

<?php require_once './layout/footer.php'; ?>