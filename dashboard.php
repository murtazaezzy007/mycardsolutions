<?php require_once './layout/header.php'; ?>
    <div class="mypage-header">
        <div class="container">
            <div class="row account-top-section">
                <div class="col-md-2 p-r-0 accounts-time-wrapper-container">
                    <p class="accounts-time-wrapper"><b class="accounts-time">03/10/2018 | 3:48 PM IST</b></p>
                </div>
                <div class="col-md-9 accounts-topic-container">
                    <div class="well accounts-topic text-center">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                    </div>
                </div>
                <div class="col-md-1 logout-button-container">
                    <button type="button" class="btn btn-danger logout-button">LOG OUT</button>
                </div>
            </div>
        </div><!-- /.container -->
    </div>


    <div class="container m-t-15 m-b-100">
        <div class="row m-b-10">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.php">Home</a></li>
                    <li class="active">My Accounts & Profile</li>
                </ol>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-12 account-id-details">
               <p>&nbsp;&nbsp;Welcome, <b class="text-primary">Ms. Geetha Shetty</b></p>
            </div>
            <div class="col-md-6 col-sm-8 col-xs-12 account-id-details">
                <b>CUSTOMER ID</b> &nbsp;&nbsp; 0000021391283 &nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;<b>MEMBER ID</b> &nbsp;&nbsp; RB0213918230239189
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
            <?php require_once './layout/user-sidebar.php'; ?>
            </div>
            <div class="col-md-9">
                <div class="accounts-title">
                    Accounts
                </div>
                <div class="table-responsive">
                <table class="table table-bordered accounts-table">
                    <thead>
                        <tr>
                            <th>Type</th>
                            <th>Number</th>
                            <th>Name</th>
                            <th>clear balance</th>
                            <th>unclear balance</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Savings Bank</td>
                            <td>SBO2391838i4893248</td>
                            <td>XYZ Name 08923</td>
                            <td>Rs. 60,000.00</td>
                            <td>Rs. 00.00</td>
                        </tr>
                        <tr>
                            <td>Fixed Deposit</td>
                            <td>SBO2391838i4893248</td>
                            <td>XYZ Name 08923</td>
                            <td>Rs. 60,000.00</td>
                            <td>Rs. 00.00</td>
                        </tr>
                        <tr>
                            <td>Loan</td>
                            <td>SBO2391838i4893248</td>
                            <td>XYZ Name 08923</td>
                            <td>Rs. 60,000.00</td>
                            <td>Rs. 00.00</td>
                        </tr>
                        <tr>
                            <td>Locker</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Share/Membership</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="no-pad">
                                <button type="buttton" class="btn btn-primary">View Master Information</button>
                            </td>
                            <td></td>
                            <td class="no-pad">
                                <button type="buttton" class="btn btn-primary">Pass Sheet</button>
                            </td>
                            <td class="no-pad">
                                <button type="buttton" class="btn btn-primary">View</button>
                            </td>
                            <td class="no-pad">
                                <button type="buttton" class="btn btn-primary">Download</button>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                </div>
                
            </div>
        </div>
    </div>
    

    
    
    
<?php require_once './layout/footer.php'; ?>