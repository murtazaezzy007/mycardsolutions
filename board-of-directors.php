<?php require_once './layout/header.php'; ?>
<?php
    $staff_members = array(
        array(
            'name' => 'SRI AMBIKAPATHY SETTY D H',
            'role' => 'GENERAL MANAGER',
            'contact' => '98801 32316',
            'image'=> 'assets/images/staff/img1.png'
        ),
        array(
            'name' => 'SRI BHEEMANANDA SETTY',
            'role' => 'BRANCH MANAGER',
            'contact' => '95135 63421',
            'image'=> 'assets/images/staff/img2.png'
        ),
        array(
            'name' => 'SRI PRASAD P V',
            'role' => 'BRANCH MANAGER',
            'contact' => '95135 63422',
            'image'=> 'assets/images/staff/img3.png'
        ),
        array(
            'name' => 'SMT PARVATHAMMA R L',
            'role' => 'BRANCH MANAGER',
            'contact' => '95135 63423',
            'image'=> 'assets/images/staff/img4.png'
        ),
        array(
            'name' => 'SRI VIJAYA KUMAR S R',
            'role' => 'BRANCH MANAGER',
            'contact' => '95135 63424',
            'image'=> 'assets/images/staff/img5.png'
        ),
        array(
            'name' => 'SRI PRASAD SETTY N',
            'role' => 'BRANCH MANAGER',
            'contact' => '95135 63425',
            'image'=> 'assets/images/staff/img6.png'
        ),
        array(
            'name' => 'SRI SANDESHA TEEKA',
            'role' => 'SPECIAL ASST. OFFICER',
            'contact' => '95135 63433',
            'image'=> 'assets/images/staff/img7.png'
        ),
        array(
            'name' => 'SMT SHANTHALA B A',
            'role' => 'SPECIAL ASST. OFFICER',
            'contact' => '95135 63426',
            'image'=> 'assets/images/staff/img8.png'
        ),
        array(
            'name' => 'SMT NAGAJYOTHI',
            'role' => 'SPECIAL ASST. OFFICER',
            'contact' => '95135 63427',
            'image'=> 'assets/images/staff/img9.png'
        ),
        array(
            'name' => 'SRI NANJUNDESHWARA M V',
            'role' => 'SPECIAL ASST. OFFICER',
            'contact' => '95135 63429',
            'image'=> 'assets/images/staff/img10.png'
        ),
        array(
            'name' => 'SRI SUBRAMANYA A P',
            'role' => 'JUNIOR CLERK',
            'contact' => '95135 63431',
            'image'=> 'https://via.placeholder.com/210x242'
        ),
        array(
            'name' => 'SRI MALLESH B J',
            'role' => 'JUNIOR CLERK',
            'contact' => '95135 63428',
            'image'=> 'https://via.placeholder.com/210x242'
        ),
        array(
            'name' => 'SRI TEJASWI K R',
            'role' => 'JUNIOR CLERK',
            'contact' => '81050 89968',
            'image'=> 'https://via.placeholder.com/210x242'
        ),
        array(
            'name' => 'SMT KAVITHA',
            'role' => 'JUNIOR CLERK',
            'contact' => '9513563430',
            'image'=> 'https://via.placeholder.com/210x242'
        ),
        array(
            'name' => 'SMT TEJASWINI K P',
            'role' => 'JUNIOR CLERK',
            'contact' => '95916 91214',
            'image'=> 'https://via.placeholder.com/210x242'
        ),
        array(
            'name' => 'SRI VISHWANATHA C',
            'role' => 'JUNIOR CLERK',
            'contact' => '85536 59220',
            'image'=> 'https://via.placeholder.com/210x242'
        ),
        array(
            'name' => 'SRI RANGANATHA G',
            'role' => 'JUNIOR CLERK',
            'contact' => '95135 63420',
            'image'=> 'https://via.placeholder.com/210x242'
        ),
        array(
            'name' => 'SRI HARSHA K N',
            'role' => 'JUNIOR CLERK',
            'contact' => '81234 63616',
            'image'=> 'https://via.placeholder.com/210x242'
        ),
        array(
            'name' => 'SRI RAKESHA',
            'role' => 'JUNIOR CLERK',
            'contact' => '95386 04438',
            'image'=> 'https://via.placeholder.com/210x242'
        ),
        array(
            'name' => 'SRI SACHIN Y R',
            'role' => 'JUNIOR CLERK',
            'contact' => '89512 28861',
            'image'=> 'https://via.placeholder.com/210x242'
        ),
        array(
            'name' => 'SRI ARUNA R',
            'role' => 'JUNIOR CLERK',
            'contact' => '72046 16772',
            'image'=> 'https://via.placeholder.com/210x242'
        ),
        array(
            'name' => 'SRI TEJAS GUPTA K S',
            'role' => 'JUNIOR CLERK',
            'contact' => '76192 61015',
            'image'=> 'https://via.placeholder.com/210x242'
        ),
        array(
            'name' => 'SRI KHARTE L R',
            'role' => 'PEON',
            'contact' => '72046 16772',
            'image'=> 'https://via.placeholder.com/210x242'
        ),
        array(
            'name' => 'SRI NAGARAJ N',
            'role' => 'PEON',
            'contact' => '95135 63432',
            'image'=> 'https://via.placeholder.com/210x242'
        ),
        array(
            'name' => 'SRI SHABBIR AHAMED',
            'role' => 'PEON CUM DRIVER',
            'contact' => '76192 61011',
            'image'=> 'https://via.placeholder.com/210x242'
        ),
        array(
            'name' => 'SRI MADHU C',
            'role' => 'PEON',
            'contact' => '76192 61013',
            'image'=> 'https://via.placeholder.com/210x242'
        ),
        array(
            'name' => 'SRI MANJUNATHA SETTY V M',
            'role' => 'PEON',
            'contact' => '76192 61014',
            'image'=> 'https://via.placeholder.com/210x242'
        )
    );
    $board_of_directors = array(
        array(
            'image'=> 'assets/images/board-of-directors/img1.png',
            'name' => 'SRI SRINIVASAMURTHY R G',
            'role' => 'PRESIDENT',
            'contact' => '88840 99999'
        ),
        array(
            'image'=> 'assets/images/board-of-directors/img2.png',
            'name' => 'SRI NAGABUSHAN B P',
            'role' => 'VICE PRESIDENT',
            'contact' => '90085 32747'
        ),
        array(
            'image'=> 'assets/images/board-of-directors/img3.png',
            'name' => 'SRI PRABHAKAR R L',
            'role' => 'DIRECTOR',
            'contact' => '94480 45683'
        ),
        array(
            'image'=> 'assets/images/board-of-directors/img4.png',
            'name' => 'SRI KASAL S SATISH',
            'role' => 'DIRECTOR',
            'contact' => '99640 21175'
        ),
        array(
            'image'=> 'assets/images/board-of-directors/img5.png',
            'name' => 'SRI SESHADRIPRASAD C P',
            'role' => 'DIRECTOR',
            'contact' => '94481 29243'
        ),
        array(
            'image'=> 'assets/images/board-of-directors/img6.png',
            'name' => 'SRI MANJUNATH B N',
            'role' => 'DIRECTOR',
            'contact' => '94485 89498'
        ),
        array(
            'image'=> 'assets/images/board-of-directors/img7.png',
            'name' => 'SRI SATISH KUMAR C P',
            'role' => 'DIRECTOR',
            'contact' => '94480 43470'
        ),
        array(
            'image'=> 'assets/images/board-of-directors/img8.png',
            'name' => 'SRI RAGHUNATH A S',
            'role' => 'DIRECTOR',
            'contact' => '98441 27913'
        ),
        array(
            'image'=> 'assets/images/board-of-directors/img9.png',
            'name' => 'SRI KASHINATH N',
            'role' => 'DIRECTOR',
            'contact' => '94489 29493'
        ),
        array(
            'image'=> 'assets/images/board-of-directors/img10.png',
            'name' => 'SMT GEETHA R B',
            'role' => 'FEMALE DIRECTOR',
            'contact' => '94481 54660'
        ),
        array(
            'image'=> 'assets/images/board-of-directors/img11.png',
            'name' => 'SMT SUJATHA R N',
            'role' => 'FEMALE DIRECTOR',
            'contact' => '94813 63757'
        ),
        array(
            'image'=> 'assets/images/board-of-directors/img12.png',
            'name' => 'SRI SRIDHAR G',
            'role' => 'PROFESSIONAL DIRECTOR',
            'contact' => '94481 10189'
        ),
        array(
            'image'=> 'assets/images/board-of-directors/img13.png',
            'name' => 'SRI NAGARAJA SETTY R',
            'role' => 'PROFESSIONAL DIRECTOR',
            'contact' => '94817 20652'
        )
    );
?>

<div class="top-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
            <h2 class="banner-text">Our TRUST, <br>
    is our foundation.</h2>
            </div>
        </div>
    </div>

        </div>
    
    <div class="container m-t-20 m-b-40">
        <div class="row">
            <div class="col-md-12">
            <h4>BOARD OF DIRECTORS</h4>
            <br>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
                
                
                <div class="row">
                <?php 
                    for($i = 0; $i < count($board_of_directors); $i++):
                    ?>
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <div class="m-b-30">
                            <img src="<?php echo $board_of_directors[$i]['image']; ?>" class="img img-responsive m-b-10" alt="<?php echo $board_of_directors[$i]['name']; ?>">
                            <p class="f-w-600 f-s-16"><?php echo $board_of_directors[$i]['name']; ?></p>
                            <p><?php echo $board_of_directors[$i]['role']; ?></p>
                            <p><?php echo $board_of_directors[$i]['contact']; ?></p>
                        </div>
                    </div>
                    <?php endfor; ?>
                </div>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-12">
                <?php require_once './layout/newsboard.php'; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
            <h4>BANK OFFICERS AND STAFF</h4>
                <br>
                <div class="row">
                <?php 
                    for($i = 0; $i < count($staff_members); $i++):
                    ?>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="m-b-30">
                            <img src="<?php echo $staff_members[$i]['image']; ?>" class="img img-responsive m-b-10" alt="<?php echo $staff_members[$i]['name']; ?>">
                            <p class="f-w-600 f-s-16"><?php echo $staff_members[$i]['name']; ?></p>
                            <p><?php echo $staff_members[$i]['role']; ?></p>
                            <p><?php echo $staff_members[$i]['contact']; ?></p>
                        </div>
                    </div>
                    <?php endfor; ?>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <table class="table table-bordered customised-table m-t-22">
                            <tbody>
                                <tr>
                                    <th class="col-md-4">Name</th>
                                    <th class="col-md-4">Position</th>
                                    <th class="col-md-4">Mobile No.</th>
                                </tr>
                                <?php 
                                for($i = 0; $i < count($staff_members); $i++):
                                ?>
                                <tr>
                                    <td><?php echo $staff_members[$i]['name']; ?></td>
                                    <td><?php echo $staff_members[$i]['role']; ?></td>
                                    <td><?php echo $staff_members[$i]['contact']; ?></td>
                                </tr>
                                <?php endfor; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php require_once './layout/footer.php'; ?>