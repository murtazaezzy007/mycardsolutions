<?php 
    $active = 'current-account';
?>
<?php require_once './layout/header.php'; ?>

    <div class="container m-t-20 m-b-40">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Deposit Products</a></li>
                    <li class="active">Current Account</li>
                </ol>
            </div>
            <div class="col-md-3 col-xs-12">
                <?php require_once './layout/nav-sidebar.php'; ?>
            </div>
            <div class="col-md-9 content col-xs-12">
                <h1>Current Account</h1>
                <div class="btn-sub-header-right">
                    <button type="button" class="btn btn-primary">KYC Guidelines</button>
                </div>
                <table class="table table-bordered customised-table m-t-22">
                    <tbody>
                        <tr>
                            <th>Purpose</th>
                            <td>Accounts are opened for business purpose means for other than saving purpose.</td>
                        </tr>
                        <tr>
                            <th>Minimum age to account holders.</th>
                            <td>18 Years completed as on the date of account opening.</td>
                        </tr>
                        <tr>
                            <th>Minimum amount to open an account.</th>
                            <td>Rs.5000/-</td>
                        </tr>
                        <tr>
                            <th>Rate of interest.</th>
                            <td>No interest is paid.</td>
                        </tr>
                        <tr>
                            <th>TDS applicability</th>
                            <td>Not applicable as interest is not paid by the Bank.</td>
                        </tr>
                        <tr>
                            <th>Form 15G/H</th>
                            <td>Not applicable as interest is not paid by the Bank.</td>
                        </tr>
                        <tr>
                            <th>Nomination Facilities.</th>
                            <td></td>
                        </tr>
                        <tr>
                            <th>Type of A/C</th>
                            <td>Current Account.</td>
                        </tr>
                        <tr>
                            <th>Minimum Balance</th>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php require_once './layout/footer.php'; ?>