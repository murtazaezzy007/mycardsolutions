<ul class="list-group sidebar">
    <li class="list-group-item <?php if($active === 'savings-account') { echo 'active';} ?>"><a href="savings-account.php">Savings Bank Account</a></li>
    <li class="list-group-item <?php if($active === 'current-account') { echo 'active';} ?>"><a href="current-account.php">Current Account</a></li>
    <li class="list-group-item <?php if($active === 'fixed-deposit') { echo 'active';} ?>"><a href="fixed-deposit.php">Fixed Deposit <span class="sub-text">(Simple Interest Monthly/ Quarterly)</span></a></li>
    <li class="list-group-item <?php if($active === 'cash-certificate-deposit') { echo 'active';} ?>"><a href="cash-certificate-deposits.php">Cash Certificate Deposit <span class="sub-text">(Cumulative Interest)</span></a></li>
    <li class="list-group-item <?php if($active === 'recurring-deposit') { echo 'active';} ?>"><a href="recurring-deposit.php">Recurring Deposit</a></li>
    <li class="list-group-item <?php if($active === 'pigmy-deposit') { echo 'active';} ?>"><a href="pigmy-deposit.php">Pigmy Deposit</a></li>
    <li class="list-group-item <?php if($active === 'sindoor-deposit') { echo 'active';} ?>"><a href="coming-soon.php">Sindhoor Deposit</a></li>
    <li class="list-group-item <?php if($active === 'sindoor-deposit') { echo 'active';} ?>"><a href="coming-soon.php">Basic Savings Bank A/C</a></li>
</ul>