<div class="newsboard-wrapper">
    <h4>NEWS BOARD</h4>
    <ol class="list-unstyled">
        <li>
            <div class="newsboard-number"><b>01</b></div>
            <div class="newsboard-description">
                <b>Consectetur adipiscing elit, sed do eiusmod tempor</b>
                <p class="sub-description">Enim ad minim veniam, lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</p>
            </div>
        </li>
        <li>
            <div class="newsboard-number"><b>01</b></div>
            <div class="newsboard-description">
                <b>Consectetur adipiscing elit, sed do eiusmod tempor</b>
                <p class="sub-description">Enim ad minim veniam, lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</p>
            </div>
        </li>
        <li>
            <div class="newsboard-number"><b>01</b></div>
            <div class="newsboard-description">
                <b>Consectetur adipiscing elit, sed do eiusmod tempor</b>
                <p class="sub-description">Enim ad minim veniam, lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</p>
            </div>
        </li>
    </ol>
</div>