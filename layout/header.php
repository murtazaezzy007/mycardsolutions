<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Shri Kannikaparameshwari Co-operative Bank</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/unix.css">
    <link rel="stylesheet" href="assets/css/style.css">
    
    <link rel="stylesheet" href="assets/css/style-tablet.css" media="screen and (min-width: 768px) and (max-width: 991px)">
    <link rel="stylesheet" href="assets/css/style-mobile-and-above.css" media="screen and (min-width: 320px)">
    <link rel="stylesheet" href="assets/css/style-tablet-and-above.css" media="screen and (min-width: 768px)">
    <link rel="stylesheet" href="assets/css/style-medium-and-above.css" media="screen and (min-width: 991px)">
    <link rel="stylesheet" href="assets/css/style-desktop.css" media="screen and (min-width: 1200px)">
    <link rel="stylesheet" href="assets/css/style-phablet.css" media="screen and (max-width: 767px)">
    <link rel="stylesheet" href="assets/css/style-mobile.css" media="screen and (max-width: 480px)">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" href="assets/css/animate.css"> -->
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
  <div class="container p-t-10 p-b-10">
    <div class="row">
      <div class="col-md-5 col-md-offset-1 col-sm-7  col-sm-offset-2 col-xs-offset-2 col-xs-10">
        <h1 class="company-name hidden-xs">Sri Kannikaparameshwari Corporative Bank</h1>
      </div>
      <div class="col-md-6 col-md-offset-0 col-sm-3 col-sm-offset-0 col-xs-offset-2 col-xs-10 text-right">
        <!-- <div class="row"> -->
          <!-- <div class="col-md-6"><b>HEAD OFFICE:</b>
          # 165/1-2, 3rd Main, P J Extension Davangere – 577002
          </div> -->
          <!-- <div class="col-md-6"> -->
          <strong class="f-w-600">CALL US:</strong> <br>
          08192-257260, 251653
          <!-- </div> -->
        <!-- </div> -->
      </div>
    </div>
      
  </div>

  <nav class="navbar navbar-default">
    <div class="container">
      <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">
            <!-- Project name -->
            <img src="assets/images/logo-50th-anniversary.png" alt="Logo" class="logo">
            
          </a>
      </div>
      <div id="navbar" class="collapse navbar-collapse">
        <ul class="nav navbar-nav navbar-right">
          <li class="active"><a href="index.php">Home</a></li>
          <li><a href="coming-soon.php">History</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" id="drop2" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> About Us <span class="caret"></span> </a> 
            <ul class="dropdown-menu" aria-labelledby="drop2">
              <li><a href="board-of-directors.php">Board of directors</a></li>
              <!-- <li><a href="#">Another action</a></li> -->
              <!-- <li><a href="#">Something else here</a></li> -->
              <!-- <li role="separator" class="divider"></li> -->
              <!-- <li><a href="#">Separated link</a></li> -->
            </ul> 
          </li>
          <li><a href="branch-networks.php">Branch Networks</a></li>
          <li><a href="login.php">Customer Login</a></li>
          <li><hr></li>
          <li class="dropdown blue visible-xs">
            <a href="#" class="dropdown-toggle" id="drop3" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Products <span class="caret"></span> </a> 
            <ul class="dropdown-menu" aria-labelledby="drop3">
              <li><a href="savings-account.php">Deposit Products</a></li>
              <li><a href="loan-products.php">Loan Products</a></li>
              <li><a href="coming-soon.php">Other Products</a></li>
              <li><a href="coming-soon.php">Bank Functions</a></li>
            </ul> 
          </li>
          <li><a href="services.php" class="visible-xs">Services</a></li>
          <li><a href="coming-soon.php" class="visible-xs">Requests</a></li>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </nav>

      <div class="sub-header hidden-xs">
        <div class="container">
        <div class="row sub-header-contents">
          <div class="col-md-2">
            <!-- <a href="savings-account.php">Deposit Products</a> -->
             <div class="dropdown">
              <a href="#" class="dropdown-toggle" id="drop3" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Deposit Products <span class="caret"></span> </a> 
              <ul class="dropdown-menu" aria-labelledby="drop3"> 
                <li><a href="savings-account.php">Savings Bank Account</a></li>
                <li><a href="current-account.php">Current Account</a></li>
                <li><a href="fixed-deposit.php">Fixed Deposit <span class="sub-text">(Simple Interest Monthly/ Quarterly)</span></a></li>
                <li><a href="cash-certificate-deposits.php">Cash Certificate Deposit <span class="sub-text">(Cumulative Interest)</span></a></li>
                <li><a href="recurring-deposit.php">Recurring Deposit</a></li>
                <li><a href="pigmy-deposit.php">Pigmy Deposit</a></li>
                <li><a href="coming-soon.php">Sindhoor Deposit</a></li>
                <li><a href="coming-soon.php">Basic Savings A/C</a></li>
              </ul> 
            </div>
          </div>
          <div class="col-md-2">
            <a href="loan-products.php">Loan Products</a>
          </div>
          <div class="col-md-2">
            <a href="coming-soon.php">Other Products</a>
          </div>
          <div class="col-md-2">
            <a href="services.php">Services</a>
          </div>
          <div class="col-md-2">
            <a href="coming-soon.php">Requests</a>
          </div>
          <div class="col-md-2">
            <a href="coming-soon.php">Bank Functions</a>
          </div>
        </div>
          <!-- <div class="sub-header-contents">
           
            <a href="savings-account.php">
              Deposit Products
              
            </a>
            <a href="loan-products.php">
              Loan Products
            </a>
            <a href="coming-soon.php">
              Other Products
              
            </a>
            <a href="services.php">
              Services
              
            </a>
            <a href="coming-soon.php">
              Requests
              
            </a>
          </div> -->
        </div>
      </div>