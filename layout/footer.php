<div style="text-align: right;padding-right: 20px;" class="<?php if(isset($hideFooterImg) && $hideFooterImg){echo "hidden";} ?>">
  <img src="assets/images/court.png" alt="">
</div>

<div class="custom-overlay animated">
<div class="lds-ripple"><div></div><div></div></div>
</div>

<div class="sub-footer">
  <div class="container">
    <div class="sub-footer-contents">
      <a href="deposits-roi.php">Deposits - Rate of interest</a>
      <a href="loans-roi.php">Loan - Rate of interest</a>
      <a href="service-charges.php">Service charges</a>
      <a href="coming-soon.php">Gallery</a>
    </div>
  </div>
</div>

<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
          <div class="row">
          <div class="col-md-12">
            <h4>Head office</h4>
          </div>
          <div class="col-md-8 col-xs-7">
            <p># 165/1-2, Srivaravi Road, 3rd Main, P J Extension Davangere – 577002</p>
            <p>TEL - 08192-257260, 251653</p>
            <p>MAIL  - skpcb.web@gmail.com</p>
            <!-- <br>
            <div class="social-links visible-xs">
              <a href="#"><i class="fa fa-facebook"></i></a>
              <a href="#"><i class="fa fa-twitter"></i></a>
              <a href="#"><i class="fa fa-instagram"></i></a>
            </div> -->
            
          </div>
          <div class="col-md-4 col-xs-5">
            <ul class="list-unstyled">
              <li><a href="coming-soon.php">TERMS OF USE</a></li>
              <li><a href="coming-soon.php">PRIVACY POLICY</a></li>
              <li><a href="coming-soon.php">CAREERS</a></li>
              <li><a href="coming-soon.php">DOWNLOADS</a></li>
            </ul>
          </div>
          <div class="col-md-12 col-xs-12">
            
            <div class="social-links">
              <a href="#"><i class="fa fa-facebook"></i></a>
              <a href="#"><i class="fa fa-twitter"></i></a>
              <a href="#"><i class="fa fa-instagram"></i></a>
            </div>
            <hr class="visible-xs">
          </div>
        </div>
      </div>

      <div class="col-md-6">
          <div class="row">
          <div class="col-md-12">
            <h4>Contact Us</h4>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Name">
            </div>
            <div class="form-group">
              <input type="email" class="form-control" placeholder="E-Mail ID">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Subject">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <textarea class="form-control" placeholder="Message" style="height: 83px;"></textarea>
            </div>
            <div class="form-group">
              <button class="btn btn-primary">Submit</button>
            </div>
          </div>

        </div>
      </div>
    </div>
    
  </div>
</footer>

<div class="copyright">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-12 text-left mob-text-center">
        <p>Last updated on: 24th February 2019</p>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12 text-right mob-text-center">
        Copyright @ 2018 Sri Kannikaparameshwari Co-op Bank pvt limited
      </div>
    </div>
  </div>

</div>

   
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="assets/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js" ></script>

    <script>
      $(document).ready(function() {
        $('.custom-overlay').addClass('fadeOut');
        setTimeout(() => {
          $('.custom-overlay').hide();
        }, 1000);
      })
    </script>
  </body>
</html>