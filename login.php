<?php require_once './layout/header.php'; ?>
    <div class="mypage-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li class="active">Login</li>
                    </ol>

                    <h1>Login to online banking</h1>
                    <hr class="short orange">
                </div>
            </div>
        </div><!-- /.container -->
    </div>


    <div class="container m-t-40 p-b-40">
        <div class="row">
            <div class="col-md-5 col-sm-6 col-xs-12">
                <p><b>CASE</b> &nbsp;&nbsp; Username and password are case sensitive.</p>
                <form action="#" class="login-form">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" id="username" name="username" class="form-control" placeholder="Enter Username" required maxlength="100">
                    </div>
                    <div class="form-group">
                        <label for="username">Password</label>
                        <input type="text" id="password" name="password" class="form-control" placeholder="Enter Password" required maxlength="100">
                    </div>
                    <div class="login-options form-group text-right">
                        <a href="#" class="m-r-5">Forgot Password?</a>
                        <a href="#">Forgot Username?</a>
                    </div>
                    <div class="login-buttons">
                        <a href="dashboard.php" class="btn btn-primary text-uppercase">Login</a>
                        <!-- <button type="submit" class="btn btn-primary">Login</button> -->
                        <button type="reset" class="btn btn-primary text-uppercase">Reset</button>
                    </div>
                </form>
            </div>
            <div class="col-md-7 col-sm-6 col-xs-12">
                <img src="assets/images/7925777110c3bae37d6aeeec6010aaa0eef49b9a.jpg" class="img img-responsive login-image" alt="Login Image">
            </div>
        </div>
    </div>
    

    
    
    
<?php require_once './layout/footer.php'; ?>