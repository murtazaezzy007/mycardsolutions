<?php require_once './layout/header.php'; ?>
    <div class="mypage-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li class="active">Loan Products</li>
                    </ol>

                    <h1>Loan Products</h1>
                    <hr class="short orange">
                    <div class="btn-header-right">
                    <button type="button" class="btn btn-primary">EMI Calculator</button>
                    </div>
                    
                </div>
            </div>
        </div><!-- /.container -->
    </div>

    <div class="container m-t-40 m-b-40">
        <div class="row">
        <?php 
            $arr = array(
                array('name' => 'Loan on FD', 'image' => 'assets/images/tmpnull.jpg'), 
                array('name' => 'Loan on Gold Jewellery', 'image' => 'assets/images/tmpnull.jpg'), 
                array('name' => 'Salary Loan', 'image' => 'assets/images/tmpnull.jpg'), 
                array('name' => 'Business Loan', 'image' => 'assets/images/tmpnull.jpg'), 
                array('name' => 'Personal Loan', 'image' => 'assets/images/tmpnull.jpg'), 
                array('name' => 'Vehicle Loan', 'image' => 'assets/images/tmpnull.jpg'), 
                array('name' => 'Housing Loan', 'image' => 'assets/images/tmpnull.jpg'), 
                array('name' => 'Mortgage Loan', 'image' => 'assets/images/tmpnull.jpg'), 
                array('name' => 'Overdraft', 'image' => 'assets/images/tmpnull.jpg'), 
                array('name' => 'Cash Credit', 'image' => 'assets/images/tmpnull.jpg'), 
                array('name' => 'Open Trust Loan', 'image' => 'assets/images/tmpnull.jpg'), 
                array('name' => 'Un-secured Loan', 'image' => 'assets/images/tmpnull.jpg')
            );
            for($i = 0; $i < count($arr); $i++):
            ?>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <figure class="loan-product">
                    <img src="<?php echo $arr[$i]['image']; ?>" class="img img-responsive" alt="Loan product image">
                    <figcaption><?php echo $arr[$i]['name']; ?></figcaption>
                </figure>
            </div>
            <?php endfor; ?>
        </div>
    </div>
    
<?php require_once './layout/footer.php'; ?>