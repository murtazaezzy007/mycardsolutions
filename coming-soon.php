<?php require_once './layout/header.php'; ?>
    <div class="mypage-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li class="active">About us</li>
                    </ol>

                    <h1>This page is under construction</h1>
                    <hr class="short orange">
                </div>
            </div>
        </div><!-- /.container -->
    </div>


    <div class="container m-t-40">
        <div class="row">
            <div class="col-md-12">
                <p class="f-s-18">Welcome to Sri Kannikaparameshwari Co-Op Bank Ltd. We are currently working on this page. </p>
                <a href="index.php" class="btn btn-primary">Go back to Home</a>
            </div>
        </div>
    </div>
    

<?php require_once './layout/footer.php'; ?>