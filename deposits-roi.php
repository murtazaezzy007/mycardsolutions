<?php require_once './layout/header.php'; ?>
<?php
    $deposits = array(
        array(
            'from' => '0 Days',
            'to' => '14 Days.',
            'rate' => '0%',
        ),
        array(
            'from' => '15 Days',
            'to' => '45 Days.',
            'rate' => '4%',
        ),
        array(
            'from' => '46 Days',
            'to' => '90 Days.',
            'rate' => '4%',
        ),
        array(
            'from' => '91 Days',
            'to' => '180 Days.',
            'rate' => '5%',
        ),
        array(
            'from' => '181 Days',
            'to' => '1 Year',
            'rate' => '6%',
        ),
        array(
            'from' => 'Above 1 Year',
            'to' => '2 Years',
            'rate' => '7.5%',
        ),
        array(
            'from' => 'Above 2 Years',
            'to' => '3 Years',
            'rate' => '7.75%'
        ),
        array(
            'from' => 'Above 3 Years',
            'to' => '',
            'rate' => '7.5%'
        ),
        array(
            'from' => 'For Seniors Citizen’s',
            'to' => '',
            'rate' => '0.50% extra'
        )
    );
?>

    <div class="mypage-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li class="active">Deposits</li>
                    </ol>

                    <h1>Deposits - Rate of Interest</h1>
                    <hr class="short orange">
                </div>
            </div>
        </div><!-- /.container -->
    </div>


    <div class="container m-t-40 m-b-40">
        <div class="row">
            <div class="col-md-8">
                <table class="table table-bordered customised-table">
                    <tbody>
                        <tr>
                            <th colspan="2">Period</th>
                            <th colspan="2"><b>Rate of Interest</b></th>
                        </tr>
                        <tr>
                            <th><b>From</b></th>
                            <th><b>To</b></th>
                            <th><b>Effective From</b></th>
                            <th><b>01.12.2016</b></th>
                        </tr>
                        <?php 
                            for($i = 0; $i < count($deposits); $i++):
                        ?>
                        <tr>
                            <td><?php echo $deposits[$i]['from']; ?></td>
                            <td><?php echo $deposits[$i]['to']; ?></td>
                            <td colspan="2"><?php echo $deposits[$i]['rate']; ?></td>
                        </tr>
                        <?php endfor; ?>
                    </tbody>
                </table>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <?php require_once './layout/newsboard.php'; ?>
            </div>
        </div>
    </div>
    

<?php require_once './layout/footer.php'; ?>