<?php require_once './layout/header.php'; ?>
    <div class="mypage-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li class="active">Services</li>
                    </ol>

                    <h1>SERVICES</h1>
                    <hr class="short orange">
                </div>
            </div>
        </div><!-- /.container -->
    </div>


    <div class="container m-t-40 m-b-40">
        <div class="row">
            <?php 
            $arr = array('SMS SERVICES','Internet Banking
            Request Services', 'Locker Services', 'DD/PO Services.', 'Pass Book', 'Pass Sheet', 'RuPay Debit 
            Cards', 'RTGS/NEFT', 'Standing
            Instructions', 'CTS Clg', 'CKYC facilities', 'D.B.T. Facility', 'e-payments', 'General 
            Insurance', 'E.C.S. facility', 'Bank Gurantee', 'TDS Payment', 'Mobile Banking', 'Savings A/c', 'E-stamp Service', 
            'Khajane - 2 (K2) Challan Creation', 'ATM Facility', 'OBC/Bills', 'Current A/c', 'Deposits, Overdraft Facility', 
            'Financial Services', 'Investments Services' );
            for($i = 0; $i < count($arr); $i++):
            ?>
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="service-box-link">
                    <div class="service-box">
                        <div class="service-no"><?php if($i<=8){ echo 0; } ?><?php echo $i+1; ?></div>
                        <div class="service-name"><?php echo $arr[$i]; ?></div>
                    </div>
                </a>
            </div>

            <?php endfor; ?>
        </div>
    </div>
    

    
    
    
<?php require_once './layout/footer.php'; ?>