<?php require_once './layout/header.php'; ?>
<?php
    $for_deposits = array(
        array(
            'name' => 'Pigme Deposit before 6 months Closing',
            'value' => '5% on O/s. Balance',
        ),
        array(
            'name' => 'Current A/c. Before 1 year Closing',
            'value' => 'Rs.100.',
        ),
        array(
            'name' => 'Non Maintenance of Minimum Balance in Current Account',
            'value' => 'Rs.10. per month',
        ),
        array(
            'name' => 'Non Maintenance of Minimum Balance in Savings Bank Account',
            'value' => 'Rs.10. per month',
        ),
        array(
            'name' => 'In operative Current A/c. Maintenance Charges',
            'value' => 'Rs.100. per Half Year',
        ),
        array(
            'name' => 'In Operative S.B. A/c. Maintenance Charges',
            'value' => 'Rs.50. Per Half Year',
        ),
        array(
            'name' => 'CTS Cheques Leaves Charges for Current, S.B. OD/CC A/c’s`',
            'value' => 'Rs.2.5. per Leaves',
        ),
        array(
            'name' => 'Cheque Reurn Charges',
            'value' => 'As applicable on Reasons',
        ),
        array(
            'name' => 'Stop Payments of Bank Cheques',
            'value' => 'As applicable on Reasons',
        ),
        array(
            'name' => 'Pigme Deposit before 6 months Closing',
            'value' => 'Stop Payments of  other Bank Cheques',
        ),
        
    );

    $for_loan = array(
        array(
            'name' => 'Loan Applications Fee below Rs.15000/-',
            'value' => 'Nil',
        ),
        array(
            'name' => 'Loan Applications Fee above Rs.15000/-',
            'value' => 'Rs.25/-',
        ),
        array(
            'name' => 'Secured Term/Operative Loans',
            'value' => 'Rs.200/-',
        ),
        array(
            'name' => 'Loan Processing Fee un-Secured <Rs.15000/-',
            'value' => 'Nil',
        ),
        array(
            'name' => 'Loan Processing Fee un-Secured /Secured Term Loans  >Rs.15000/-',
            'value' => '0.50%',
        ),
        array(
            'name' => 'Secured Operative Loans (Fresh/Renewl)',
            'value' => '0.25%',
        ),
        array(
            'name' => 'Gold Loan Processing Charges',
            'value' => '0.50%',
        ),
        array(
            'name' => 'No Due Certificate for Loan',
            'value' => 'Rs.100/-',
        ),
        array(
            'name' => 'DTD /Reconveyance  Deed Charges for All Loans',
            'value' => 'Rs.500/-',
        ),
        array(
            'name' => 'Documents Delivery Charges',
            'value' => 'Rs.250/-',
        ),
        array(
            'name' => 'Document holding above 1 year',
            'value' => 'Rs.250/-',
        ),
    );

    $others = array(
        array(
            'name' => 'SB/CA – First time issue of pass book',
            'value' => 'Nil',
        ),
        array(
            'name' => 'SB/CA – Continuation pass book.',
            'value' => 'Nil',
        ),
        array(
            'name' => 'SB/CA – Duplicate pass book with latest balance.',
            'value' => 'Rs.50.',
        ),
        array(
            'name' => 'SB/CA – Duplicate pass book with all entries. Per page.',
            'value' => 'Rs.50/- + Rs. 5/- per page',
        ),
        array(
            'name' => 'Issue of Pass Book Statement in Addition To Pass Book',
            'value' => 'Rs. 5/- Per Page',
        ),
        array(
            'name' => 'Solvency Certificate for Customers',
            'value' => 'Rs.100 upto 5 Lakhs and Rs.10.per Lakh',
        ),
        array(
            'name' => 'Bank Guarantee Charges against Deposit',
            'value' => 'Min. Rs.150. +1.5% per Lakh in Advance',
        ),
        array(
            'name' => 'Online Tax Payments',
            'value' => 'Rs.30.',
        ),
        array(
            'name' => 'DTD /Reconveyance  Deed Charges for All Loans',
            'value' => 'Rs.500/-',
        ),
        array(
            'name' => 'Online Tax Payments',
            'value' => 'Rs.30',
        ),
        array(
            'name' => 'RTGS/NEFT Charges',
            'value' => 'Minimum Rs.30. + Re.1. per Lakh',
        ),
        array(
            'name' => 'OBC Charges',
            'value' => 'Minimum Rs.50. + Rs.2. per Thousand+Postage',
        ),
        array(
            'name' => 'LBC Charges',
            'value' => 'Minimum Rs.50. + Re.1. per Thousand+Postage',
        ),
        array(
            'name' => 'DD/PO/POP Cheque Charges',
            'value' => 'Minimum Rs.15. + Re.1. per Thousand',
        ),
    );
?>    

<div class="mypage-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Library</a></li>
                        <li class="active">Data</li>
                    </ol>

                    <h1>Service Charges <small>(GST Applicable extra)</small></h1>
                    <hr class="short orange">
                </div>
            </div>
        </div><!-- /.container -->
    </div>


    <div class="container m-t-20 m-b-50">
        <div class="row">
            <div class="col-md-9 col-sm-8">
            <h4>For Deposits</h4>
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        
                        <table class="table table-bordered customised-table m-t-22">
                            <tbody>
                                <tr>
                                    <th class="col-md-6">Service Description</th>
                                    <th class="col-md-6">Service Charges</th>
                                </tr>
                                <?php 
                                for($i = 0; $i < count($for_deposits); $i++):
                                ?>
                                <tr>
                                    <td><?php echo $for_deposits[$i]['name']; ?></td>
                                    <td><?php echo $for_deposits[$i]['value']; ?></td>
                                </tr>
                                <?php endfor; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <h4>For Loans</h4>
                        <table class="table table-bordered customised-table m-t-22">
                            <tbody>
                                <tr>
                                    <th class="col-md-6">Service Description</th>
                                    <th class="col-md-6">Service Charges</th>
                                </tr>
                                <?php 
                                for($i = 0; $i < count($for_loan); $i++):
                                ?>
                                <tr>
                                    <td><?php echo $for_loan[$i]['name']; ?></td>
                                    <td><?php echo $for_loan[$i]['value']; ?></td>
                                </tr>
                                <?php endfor; ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <h4>Others</h4>
                        <table class="table table-bordered customised-table m-t-22">
                            <tbody>
                                <tr>
                                    <th class="col-md-6">Service Description</th>
                                    <th class="col-md-6">Service Charges</th>
                                </tr>
                                <?php 
                                for($i = 0; $i < count($others); $i++):
                                ?>
                                <tr>
                                    <td><?php echo $others[$i]['name']; ?></td>
                                    <td><?php echo $others[$i]['value']; ?></td>
                                </tr>
                                <?php endfor; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="m-t-62 hidden-xs"></div>
                <?php require_once './layout/newsboard.php'; ?>
            </div>
            
        </div>
    </div>
    

    
    
    
<?php require_once './layout/footer.php'; ?>