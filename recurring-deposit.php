<?php 
    $active = 'recurring-deposit';
?>
<?php require_once './layout/header.php'; ?>

    <div class="container m-t-20 m-b-40">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Deposit Products</a></li>
                    <li class="active">Recurring Deposit</li>
                </ol>
            </div>
            <div class="col-md-3 col-xs-12">
                <?php require_once './layout/nav-sidebar.php'; ?>
            </div>
            <div class="col-md-9 content col-xs-12">
                <h1>Recurring Deposit</h1>
                <div class="btn-sub-header-right">
                    <button type="button" class="btn btn-primary">Deposit Interest Calculator</button>
                </div>
                <table class="table table-bordered customised-table m-t-22">
                <tbody>
                        <tr>
                            <th>Purpose</th>
                            <td>Savings</td>
                        </tr>
                        <tr>
                            <th>Account type.</th>
                            <td>Any individual or joint, business customer.</td>
                        </tr>
                        <tr>
                            <th>Minimum age to account holders.</th>
                            <td>No age limit.</td>
                        </tr>
                        <tr>
                            <th>Minimum period of deposit</th>
                            <td>One Year.</td>
                        </tr>
                        <tr>
                            <th>Maximum period of deposit.</th>
                            <td>10 Years.</td>
                        </tr>
                        <tr>
                            <th>Minimum amount to open an account.</th>
                            <td>Rs.500/-</td>
                        </tr>
                        <tr>
                            <th>Rate of interest.</th>
                            <td>Based on period of deposit.</td>
                        </tr>
                        <tr>
                            <th>Interest calculation method.</th>
                            <td>Compounded quarterly</td>
                        </tr>
                        <tr>
                            <th>Before maturity facility.</th>
                            <td>Available with penalty as application. *</td>
                        </tr>
                        <tr>
                            <th>Nomination facility</th>
                            <td>Mandatory</td>
                        </tr>
                        <tr>
                            <th>TDS applicability</th>
                            <td>General - Rs.10000/- & above - Senior Citizen - Rs.50000/- & above</td>
                        </tr>
                        <tr>
                            <th>Form 15G/H</th>
                            <td>Accepted provided submitted within 15 days of given financial year. </td>
                        </tr>
                        <tr>
                            <th>Form 15G/H periodicity</th>
                            <td>Must be submitted during each and every new RD opening.</td>
                        </tr>
                        <tr>
                            <th>Deposit period</th>
                            <td>Step of 1 year.</td>
                        </tr>
                    </tbody>
                </table>
                <p>* T&C Apply</p>
            </div>
        </div>
    </div>

<?php require_once './layout/footer.php'; ?>